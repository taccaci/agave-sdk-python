#!/usr/bin/env python
"""
Copyright 2012 Wordnik, Inc.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
class JobSummary:
    """NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually."""


    def __init__(self):
        self.swaggerTypes = {
            'appId': 'str',
            'endTime': 'datetime',
            'executionSystem': 'str',
            'id': 'int',
            'name': 'str',
            'owner': 'str',
            'startTime': 'datetime',
            'status': 'str'

        }


        #The unique name of the application being run by this job. This must be a valid application that the calling user has permission to run.
        self.appId = None # str
        #The date the job ended in ISO 8601 format.
        self.endTime = None # datetime
        #The system id of the execution system.
        self.executionSystem = None # str
        #The unique id of the job.
        self.id = None # int
        #The name of the job.
        self.name = None # str
        #The job owner.
        self.owner = None # str
        #The date the job started in ISO 8601 format.
        self.startTime = None # datetime
        #The status of the job. Possible values are: PENDING, STAGING_INPUTS, CLEANING_UP, ARCHIVING, STAGING_JOB, FINISHED, KILLED, FAILED, STOPPED, RUNNING, PAUSED, QUEUED, SUBMITTING, STAGED, PROCESSING_INPUTS, ARCHIVING_FINISHED, ARCHIVING_FAILED
        self.status = None # str
        
