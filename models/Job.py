#!/usr/bin/env python
"""
Copyright 2012 Wordnik, Inc.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
class Job:
    """NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually."""


    def __init__(self):
        self.swaggerTypes = {
            'appId': 'str',
            'archive': 'bool',
            'archivePath': 'str',
            'archiveSystem': 'str',
            'batchQueue': 'str',
            'endTime': 'datetime',
            'executionSystem': 'str',
            'id': 'int',
            'inputs': 'JobInputs',
            'localId': 'str',
            'memoryPerNode': 'str',
            'message': 'str',
            'name': 'str',
            'nodeCount': 'int',
            'notifications': 'list[Notification]',
            'outputPath': 'String',
            'owner': 'str',
            'parameters': 'JobParameters',
            'processorsPerNode': 'int',
            'maxRunTime': 'str',
            'retries': 'int',
            'startTime': 'datetime',
            'status': 'str',
            'submitTime': 'datetime',
            'workPath': 'str'

        }


        #The unique name of the application being run by this job. This must be a valid application that the calling user has permission to run.
        self.appId = None # str
        #Whether the output from this job should be archived. If true, all new files created by this application's execution will be archived to the archivePath in the user's default storage system.
        self.archive = None # bool
        #The path of the archive folder for this job on the user's default storage sytem.
        self.archivePath = None # str
        #The unique id of the storage system on which this job's output will be staged.
        self.archiveSystem = None # str
        #The queue to which this job should be submitted. This is optional and only applies when the execution system has a batch scheduler.
        self.batchQueue = None # str
        #The date the job stopped running due to termination, completion, or error in ISO 8601 format.
        self.endTime = None # datetime
        #The system id of the execution system.
        self.executionSystem = None # str
        #The unique id of the job.
        self.id = None # int
        #The application specific input files needed for this job. These vary from application to application and should be entered as multiple individual parameters in the form. Inputs may be given as relative paths in the user's default storage system or as URI. If a URI is given, the data will be staged in by the IO service and made avaialble to the application at run time.
        self.inputs = None # JobInputs
        #The process or local job id of the job on the remote execution system.
        self.localId = None # str
        #The requested memory for this application to run given in GB.
        self.memoryPerNode = None # str
        #The error message incurred when the job failed.
        self.message = None # str
        #The name of the job.
        self.name = None # str
        #The number of processors this application should utilize while running. If the application is not of executionType PARALLEL, this should be 1.
        self.nodeCount = None # int
        #An array of notifications you wish to receive.
        self.notifications = None # list[Notification]
        #Relative path of the job's output data.
        self.outputPath = None # String
        #The job owner.
        self.owner = None # str
        #The application specific parameters needed for this job. These vary from application to application and should be entered as multiple individual parameters in the form. The actual dataType will be determined by the application description.
        self.parameters = None # JobParameters
        #The number of processors this application should utilize while running. If the application is not of executionType PARALLEL, this should be 1.
        self.processorsPerNode = None # int
        #The requested compute time needed for this application to complete given in HH:mm:ss format.
        self.maxRunTime = None # str
        #The number of retires it took to submit this job.
        self.retries = None # int
        #The date the job started in ISO 8601 format.
        self.startTime = None # datetime
        #The status of the job. Possible values are: PENDING, STAGING_INPUTS, CLEANING_UP, ARCHIVING, STAGING_JOB, FINISHED, KILLED, FAILED, STOPPED, RUNNING, PAUSED, QUEUED, SUBMITTING, STAGED, PROCESSING_INPUTS, ARCHIVING_FINISHED, ARCHIVING_FAILED
        self.status = None # str
        #The date the job was submitted in ISO 8601 format.
        self.submitTime = None # datetime
        #The directory on the remote execution system from which the job is running.
        self.workPath = None # str
        
