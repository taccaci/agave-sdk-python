#!/usr/bin/env python
"""
Copyright 2012 Wordnik, Inc.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
class ApplicationInput:
    """NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually."""


    def __init__(self):
        self.swaggerTypes = {
            'details': 'ApplicationInputDetails',
            'id': 'str',
            'order': 'ApplicationInputValue',
            'semantics': 'ApplicationInputOntology',
            'value': 'ApplicationParameterValue'

        }


        #The details for this input. 
        self.details = None # ApplicationInputDetails
        #The id of this input. This will be the replacement string in your wrapper scripts.
        self.id = None # str
        #The order in which this input should be printed when generating an execution command for forked execution. This will also be the order in which inputs are returned in the response json.
        self.order = None # ApplicationInputValue
        #The ontologies for this input. 
        self.semantics = None # ApplicationInputOntology
        #The inputs files for this input. 
        self.value = None # ApplicationParameterValue
        
