#!/usr/bin/env python
"""
WordAPI.py
Copyright 2012 Wordnik, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

NOTE: This class is auto generated by the swagger code generator program. Do not edit the class manually.
"""
import sys
import os

from models import *


class FilesApi(object):

    def __init__(self, apiClient):
      self.apiClient = apiClient

    
    def downloadFromDefaultSystem(self, sourcefilePath, **kwargs):
        """Download a file from the user's default storage location.

        Args:
            sourcefilePath, str: The path of the file relative to the user's default storage location. (required)
            
        Returns: File
        """

        allParams = ['sourcefilePath']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method downloadFromDefaultSystem" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/media/{sourcefilePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'GET'

        queryParams = {}
        headerParams = {}

        if ('sourcefilePath' in params):
            replacement = str(self.apiClient.toPathValue(params['sourcefilePath']))
            resourcePath = resourcePath.replace('{' + 'sourcefilePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'File')
        return responseObject
        
        
    def importToDefaultSsytem(self, sourcefilePath, **kwargs):
        """Import a file via direct upload or importing from a url to the user's default storage location.

        Args:
            sourcefilePath, str: The path of the file relative to the user's default storage location. (required)
            fileType, str: The file format this file is in. Defaults to raw. This will be used in file transform operations. (optional)
            callbackURL, str: The URI to notify when the import is complete. This can be an email address or http URL. If a URL is given, a GET will be made to this address. URL templating is supported. Valid template values are: ${NAME}, ${SOURCE_FORMAT}, ${DEST_FORMAT}, ${STATUS} (optional)
            fileName, str: The name of the file after importing. If not specified, the uploaded file name will be used. (optional)
            urlToIngest, str: The URL to import the file from. This parameter is used if not file is uploaded with this post. (optional)
            fileToUpload, str: The uploaded file. If not used, the urlToIngest parameter is required. (optional)
            
        Returns: SingleRemoteFileResponse
        """

        allParams = ['sourcefilePath', 'fileType', 'callbackURL', 'fileName', 'urlToIngest', 'fileToUpload']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method importToDefaultSsytem" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/media/{sourcefilePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'POST'

        queryParams = {}
        headerParams = {}

        if ('sourcefilePath' in params):
            replacement = str(self.apiClient.toPathValue(params['sourcefilePath']))
            resourcePath = resourcePath.replace('{' + 'sourcefilePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'SingleRemoteFileResponse')
        return responseObject
        
        
    def manageOnDefaultSystem(self, sourcefilePath, **kwargs):
        """Perform an action on a file or folder.

        Args:
            sourcefilePath, str: The path of the file relative to the user's default storage location. (required)
            action, str: Action to perform on the file or folder. (optional)
            filePath, str: Resulting path of the action. If a mkdir, this would be the path of the new folder relative to the current directory. If a move or copy, this is the new absolute location of the file/folder. If a rename, this is the new name of the file/folder relative to the current directory. (optional)
            
        Returns: EmptyRemoteFileResponse
        """

        allParams = ['sourcefilePath', 'action', 'filePath']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method manageOnDefaultSystem" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/media/{sourcefilePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'PUT'

        queryParams = {}
        headerParams = {}

        if ('sourcefilePath' in params):
            replacement = str(self.apiClient.toPathValue(params['sourcefilePath']))
            resourcePath = resourcePath.replace('{' + 'sourcefilePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'EmptyRemoteFileResponse')
        return responseObject
        
        
    def deleteFromDefaultSystem(self, sourcefilePath, **kwargs):
        """Deletes a file or folder.

        Args:
            sourcefilePath, str: The path of the file relative to the user's default storage location. (required)
            
        Returns: EmptyRemoteFileResponse
        """

        allParams = ['sourcefilePath']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method deleteFromDefaultSystem" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/media/{sourcefilePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'DELETE'

        queryParams = {}
        headerParams = {}

        if ('sourcefilePath' in params):
            replacement = str(self.apiClient.toPathValue(params['sourcefilePath']))
            resourcePath = resourcePath.replace('{' + 'sourcefilePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'EmptyRemoteFileResponse')
        return responseObject
        
        
    def download(self, systemId, filePath, **kwargs):
        """Download a file from the user's default storage location.

        Args:
            systemId, str: The unique id of the system on which the data resides. (required)
            filePath, str: The path of the file relative to the user's default storage location. (required)
            
        Returns: File
        """

        allParams = ['systemId', 'filePath']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method download" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/media/system/{systemId}/{filePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'GET'

        queryParams = {}
        headerParams = {}

        if ('systemId' in params):
            replacement = str(self.apiClient.toPathValue(params['systemId']))
            resourcePath = resourcePath.replace('{' + 'systemId' + '}',
                                                replacement)
        if ('filePath' in params):
            replacement = str(self.apiClient.toPathValue(params['filePath']))
            resourcePath = resourcePath.replace('{' + 'filePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'File')
        return responseObject
        
        
    def import(self, systemId, filePath, **kwargs):
        """Import a file via direct upload or importing from a url to the user's default storage location.

        Args:
            systemId, str: The unique id of the system on which the data resides. (required)
            filePath, str: The path of the file relative to the user's default storage location. (required)
            fileType, str: The file format this file is in. Defaults to raw. This will be used in file transform operations. (optional)
            callbackURL, str: The URI to notify when the import is complete. This can be an email address or http URL. If a URL is given, a GET will be made to this address. URL templating is supported. Valid template values are: ${NAME}, ${SOURCE_FORMAT}, ${DEST_FORMAT}, ${STATUS} (optional)
            fileName, str: The name of the file after importing. If not specified, the uploaded file name will be used. (optional)
            urlToIngest, str: The URL to import the file from. This parameter is used if not file is uploaded with this post. (optional)
            fileToUpload, str: The uploaded file. If not used, the urlToIngest parameter is required. (optional)
            
        Returns: SingleRemoteFileResponse
        """

        allParams = ['systemId', 'filePath', 'fileType', 'callbackURL', 'fileName', 'urlToIngest', 'fileToUpload']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method import" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/media/system/{systemId}/{filePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'POST'

        queryParams = {}
        headerParams = {}

        if ('systemId' in params):
            replacement = str(self.apiClient.toPathValue(params['systemId']))
            resourcePath = resourcePath.replace('{' + 'systemId' + '}',
                                                replacement)
        if ('filePath' in params):
            replacement = str(self.apiClient.toPathValue(params['filePath']))
            resourcePath = resourcePath.replace('{' + 'filePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'SingleRemoteFileResponse')
        return responseObject
        
        
    def manage(self, systemId, filePath, **kwargs):
        """Perform an action on a file or folder.

        Args:
            systemId, str: The unique id of the system on which the data resides. (required)
            filePath, str: The path of the file relative to the user's default storage location. (required)
            action, str: Action to perform on the file or folder. (optional)
            dirName, str: Name of new directory. Only used with the mkdir action. (optional)
            newName, str: New name of the file or folder. Only used with the rename action. (optional)
            destPath, str: Destination to which to copy the file or folder. Only used with the copy action. (optional)
            newPath, str: Destination to which to copy the file or folder. Only used with the move action. (optional)
            
        Returns: EmptyRemoteFileResponse
        """

        allParams = ['systemId', 'filePath', 'action', 'dirName', 'newName', 'destPath', 'newPath']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method manage" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/media/system/{systemId}/{filePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'PUT'

        queryParams = {}
        headerParams = {}

        if ('systemId' in params):
            replacement = str(self.apiClient.toPathValue(params['systemId']))
            resourcePath = resourcePath.replace('{' + 'systemId' + '}',
                                                replacement)
        if ('filePath' in params):
            replacement = str(self.apiClient.toPathValue(params['filePath']))
            resourcePath = resourcePath.replace('{' + 'filePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'EmptyRemoteFileResponse')
        return responseObject
        
        
    def delete(self, systemId, filePath, **kwargs):
        """Deletes a file or folder.

        Args:
            systemId, str: The unique id of the system on which the data resides. (required)
            filePath, str: The path of the file relative to the user's default storage location. (required)
            
        Returns: EmptyRemoteFileResponse
        """

        allParams = ['systemId', 'filePath']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method delete" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/media/system/{systemId}/{filePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'DELETE'

        queryParams = {}
        headerParams = {}

        if ('systemId' in params):
            replacement = str(self.apiClient.toPathValue(params['systemId']))
            resourcePath = resourcePath.replace('{' + 'systemId' + '}',
                                                replacement)
        if ('filePath' in params):
            replacement = str(self.apiClient.toPathValue(params['filePath']))
            resourcePath = resourcePath.replace('{' + 'filePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'EmptyRemoteFileResponse')
        return responseObject
        
        
    def listOnDefaultSystem(self, filePath, **kwargs):
        """Get a remote directory listing.

        Args:
            filePath, str: The path of the file relative to the user's default storage location. (required)
            
        Returns: MultipleRemoteFileResponse
        """

        allParams = ['filePath']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method listOnDefaultSystem" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/listings/{filePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'GET'

        queryParams = {}
        headerParams = {}

        if ('filePath' in params):
            replacement = str(self.apiClient.toPathValue(params['filePath']))
            resourcePath = resourcePath.replace('{' + 'filePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'MultipleRemoteFileResponse')
        return responseObject
        
        
    def list(self, systemId, filePath, **kwargs):
        """Get a remote directory listing on a specific system.

        Args:
            systemId, str: The unique id of the system on which the data resides. (required)
            filePath, str: The path of the file relative to the user's default storage location. (required)
            
        Returns: MultipleRemoteFileResponse
        """

        allParams = ['systemId', 'filePath']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method list" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/listings/system/{systemId}/{filePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'GET'

        queryParams = {}
        headerParams = {}

        if ('systemId' in params):
            replacement = str(self.apiClient.toPathValue(params['systemId']))
            resourcePath = resourcePath.replace('{' + 'systemId' + '}',
                                                replacement)
        if ('filePath' in params):
            replacement = str(self.apiClient.toPathValue(params['filePath']))
            resourcePath = resourcePath.replace('{' + 'filePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'MultipleRemoteFileResponse')
        return responseObject
        
        
    def getHistoryOnDefaultSystem(self, filePath, **kwargs):
        """Download a file from the user's default storage location.

        Args:
            filePath, str: The path of the file relative to the user's default storage location. (required)
            
        Returns: FileHistory
        """

        allParams = ['filePath']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method getHistoryOnDefaultSystem" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/history/{filePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'GET'

        queryParams = {}
        headerParams = {}

        if ('filePath' in params):
            replacement = str(self.apiClient.toPathValue(params['filePath']))
            resourcePath = resourcePath.replace('{' + 'filePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'FileHistory')
        return responseObject
        
        
    def getHistory(self, filePath, **kwargs):
        """Download a file from the user's default storage location.

        Args:
            filePath, str: The path of the file relative to the given system root location. (required)
            
        Returns: FileHistory
        """

        allParams = ['filePath']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method getHistory" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/history/system/{systemId}/{filePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'GET'

        queryParams = {}
        headerParams = {}

        if ('filePath' in params):
            replacement = str(self.apiClient.toPathValue(params['filePath']))
            resourcePath = resourcePath.replace('{' + 'filePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'FileHistory')
        return responseObject
        
        
    def listPermissionsOnDefaultSystem(self, filePath, **kwargs):
        """List all the share permissions for a file or folder.

        Args:
            filePath, str: The path of the file relative to the user's default storage location. (required)
            
        Returns: MultiplePermissionResponse
        """

        allParams = ['filePath']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method listPermissionsOnDefaultSystem" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/pems/{filePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'GET'

        queryParams = {}
        headerParams = {}

        if ('filePath' in params):
            replacement = str(self.apiClient.toPathValue(params['filePath']))
            resourcePath = resourcePath.replace('{' + 'filePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'MultiplePermissionResponse')
        return responseObject
        
        
    def updatePermissionsOnDefaultSystem(self, filePath, username, **kwargs):
        """Update permissions for a single user.

        Args:
            filePath, str: The path of the file relative to the user's default storage location. (required)
            username, str: The name of an api user for whom you are settings share permissions. (required)
            read, bool: Should read permissions be set. (optional)
            write, bool: Should write permissions be set. (optional)
            execute, bool: Should execute permissions be set. (optional)
            all, bool: Should all permissions be set. (optional)
            recursive, bool: Should updated permissions be applied recursively. Defaults to false. (optional)
            
        Returns: EmptyRemoteFileResponse
        """

        allParams = ['filePath', 'username', 'read', 'write', 'execute', 'all', 'recursive']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method updatePermissionsOnDefaultSystem" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/pems/{filePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'POST'

        queryParams = {}
        headerParams = {}

        if ('filePath' in params):
            replacement = str(self.apiClient.toPathValue(params['filePath']))
            resourcePath = resourcePath.replace('{' + 'filePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'EmptyRemoteFileResponse')
        return responseObject
        
        
    def listPermissions(self, filePath, **kwargs):
        """List all the share permissions for a file or folder.

        Args:
            filePath, str: The path of the file relative to the user's default storage location. (required)
            
        Returns: MultiplePermissionResponse
        """

        allParams = ['filePath']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method listPermissions" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/pems/system/{systemId}/{filePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'GET'

        queryParams = {}
        headerParams = {}

        if ('filePath' in params):
            replacement = str(self.apiClient.toPathValue(params['filePath']))
            resourcePath = resourcePath.replace('{' + 'filePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'MultiplePermissionResponse')
        return responseObject
        
        
    def updatePermissions(self, filePath, username, **kwargs):
        """Update permissions for a single user.

        Args:
            filePath, str: The path of the file relative to the user's default storage location. (required)
            username, str: The name of an api user for whom you are settings share permissions. (required)
            read, bool: Should read permissions be set. (optional)
            write, bool: Should write permissions be set. (optional)
            execute, bool: Should execute permissions be set. (optional)
            all, bool: Should all permissions be set. (optional)
            recursive, bool: Should updated permissions be applied recursively. Defaults to false. (optional)
            
        Returns: EmptyRemoteFileResponse
        """

        allParams = ['filePath', 'username', 'read', 'write', 'execute', 'all', 'recursive']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method updatePermissions" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/pems/system/{systemId}/{filePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'POST'

        queryParams = {}
        headerParams = {}

        if ('filePath' in params):
            replacement = str(self.apiClient.toPathValue(params['filePath']))
            resourcePath = resourcePath.replace('{' + 'filePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'EmptyRemoteFileResponse')
        return responseObject
        
        
    def deletePermissions(self, filePath, **kwargs):
        """Deletes all permissions on a file except those of the owner.

        Args:
            filePath, str: The path of the file relative to the user's default storage location. (required)
            
        Returns: EmptyRemoteFileResponse
        """

        allParams = ['filePath']

        params = locals()
        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method deletePermissions" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/files/2.0/pems/system/{systemId}/{filePath}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'DELETE'

        queryParams = {}
        headerParams = {}

        if ('filePath' in params):
            replacement = str(self.apiClient.toPathValue(params['filePath']))
            resourcePath = resourcePath.replace('{' + 'filePath' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None

        responseObject = self.apiClient.deserialize(response, 'EmptyRemoteFileResponse')
        return responseObject
        
        
    


